package at.ac.uibk.schmidstricker.securesms.sms;

import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import at.ac.uibk.schmidstricker.securesms.adapter.MessageAdapter;
import at.ac.uibk.schmidstricker.securesms.service.MessageLoader;

/**
 * Created by Benedikt on 29.10.2014.
 */
public class MessageLoaderCallback implements android.app.LoaderManager.LoaderCallbacks<List<Message>> {

    private static final String TAG = MessageLoaderCallback.class.getSimpleName();
    private final Context mContext;
    private final MessageAdapter mAdapter;

    public MessageLoaderCallback(Context context, MessageAdapter adapter) {
        this.mContext = context;
        this.mAdapter = adapter;
    }

    @Override
    public Loader<List<Message>> onCreateLoader(int i, Bundle bundle) {


        switch (i) {
            case SmsQuery.Inbox.TOKEN_INBOX:
                // This will fetch all SMS messages in the inbox, ordered by date desc
                return new MessageLoader(mContext, SmsQuery.Inbox.URI_INBOX);

            case SmsQuery.Sent.TOKEN_SENT:
                // This will fetch all SMS messages in the inbox, ordered by date desc
                return new MessageLoader(mContext, SmsQuery.Sent.URI_SENT);

            default:
                Log.e(TAG, "Unknown Loader token " + i);
                return null;


        }
    }

    @Override
    public void onLoadFinished(Loader<List<Message>> loader, List<Message> data) {

        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onLoaderReset(Loader<List<Message>> loader) {
        Log.d(TAG, "Loader " + loader + " reset");
    }

}
