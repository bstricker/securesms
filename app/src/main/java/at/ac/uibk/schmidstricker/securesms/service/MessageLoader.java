package at.ac.uibk.schmidstricker.securesms.service;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import at.ac.uibk.schmidstricker.securesms.sms.Message;
import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;

/**
 * Created by Benedikt on 28.10.2014.
 */
public class MessageLoader extends AsyncTaskLoader<List<Message>> {


    private List<Message> mMessages;
    private final Uri mUri;

    public MessageLoader(Context context, Uri uri) {
        super(context);
        mUri = uri;
    }

    @Override
    public List<Message> loadInBackground() {

        ContentResolver cr = getContext().getContentResolver();


        // Get all messages from the given Uri (Sent or Received) after the given Time
        Cursor messageCursor= cr.query(mUri, SmsQuery.Sent.PROJECTION, null, null, SmsQuery.Sent.SORT_ORDER);

        List<Message> messageList = new ArrayList<Message>();

        if (!messageCursor.moveToFirst()) {
            return messageList;
        }

        do{
            int id = messageCursor.getInt(SmsQuery.ID);
            String address = messageCursor.getString(SmsQuery.ADDRESS);
            String body = messageCursor.getString(SmsQuery.BODY);
            long date = messageCursor.getLong(SmsQuery.DATE);
            int encrypted = messageCursor.getInt(SmsQuery.ENCRYPTED);

            messageList.add(new Message(id, address, body, date, encrypted));

        } while(messageCursor.moveToNext());


        return messageList;

    }

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(List<Message> messages) {

        if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (messages != null) {
                onReleaseResources(messages);
            }
        }
        List<Message> oldMessages = mMessages;
        mMessages = messages;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(messages);
        }

        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldMessages != null) {
            onReleaseResources(oldMessages);
        }
    }

    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading() {


        if (takeContentChanged() || mMessages == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }
    /**
     * Handles a request to stop the Loader.
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(List<Message> messages) {

        super.onCanceled(messages);

        // At this point we can release the resources associated with 'messages'
        // if needed.
        onReleaseResources(messages);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset() {

        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if (mMessages != null) {
            onReleaseResources(mMessages);
            mMessages = null;
        }


    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(List<Message> messages) {
        // For a simple List<> there is nothing to do.  For something
        // like a Cursor, we would close it here.
    }

}
