package at.ac.uibk.schmidstricker.securesms.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;

import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;
import at.ac.uibk.schmidstricker.securesms.ui.activity.MainActivity;
import at.ac.uibk.schmidstricker.securesms.R;


/**
 * Created by Benedikt on 07.11.2014.
 */
public class MessageNotification {

    private static final long[] VIBRATE_PATTERN = {500, 1000, 200, 1000};

    public static final int NEW_MESSAGE_NOTIFICATION = 1;

    private static final String TAG = MessageNotification.class.getSimpleName();
    private static final int NEW_MESSAGE_ID = 1;

    public static void newMessage(Context context, Uri messageUri) {

        Cursor message = context.getContentResolver().query(messageUri, SmsQuery.Inbox.PROJECTION, null, null, null);

        if (!message.moveToFirst()) {
            Log.e(TAG, "Empty cursor for URI " + messageUri);
            return;
        }
        String address = message.getString(SmsQuery.ADDRESS);
        String body = message.getString(SmsQuery.BODY);
        long date = message.getLong(SmsQuery.DATE);

        Notification.Builder mBuilder =
                new Notification.Builder(context)
                        .setSmallIcon(R.drawable.ic_new_sms)
                        .setContentTitle(address)
                        .setContentText(body)
                        .setWhen(date)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setVibrate(VIBRATE_PATTERN)
                        .setLights(Color.argb(155, 255, 0,0), 1000, 200)
                        .setAutoCancel(true);



        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // mId allows you to update the notification later on.
        mNotificationManager.notify(NEW_MESSAGE_ID, mBuilder.build());


    }


}
