package at.ac.uibk.schmidstricker.securesms.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import at.ac.uibk.schmidstricker.securesms.CryptoUtils;
import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.Utils;
import at.ac.uibk.schmidstricker.securesms.service.MessagingService;
import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;


public class WriteMessageActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {


    private static final String TAG = WriteMessageActivity.class.getSimpleName();
    private String mSearchTerm; // Stores the current search query term

    // Whether or not the search query has changed since the last time the loader was refreshed
    private SimpleCursorAdapter mAdapter;
    private ListView mContactList;
    private EditText mEtNumber;
    private boolean mSelectContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_message);


        ImageButton buttonSend = (ImageButton) findViewById(R.id.button_send_message);
        final EditText messageBox = (EditText) findViewById(R.id.edit_message_box);
        mEtNumber = (EditText) findViewById(R.id.edit_number);

        mContactList = (ListView) findViewById(R.id.contact_list);
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.two_line_list_item, null,
                new String[]{ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER},
                new int[]{android.R.id.text1, android.R.id.text2});


        mContactList.setAdapter(mAdapter);
        mContactList.setOnItemClickListener(this);


        mEtNumber.addTextChangedListener(new AddressWatcher());


        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean cancel = false;
                View focusView = null;

                String number = mEtNumber.getText().toString();
                String msg = messageBox.getText().toString();


                // check message box first because if both fields are invalid the last one (from) will be focused
                // Message is empty; show Error
                if (msg.isEmpty()) {
                    messageBox.setError(getString(R.string.err_empty_msg));
                    focusView = messageBox;
                    cancel = true;
                }


                // max. 111 characters long because if 111 chars get encrypted, the encrypted base64 key would exceed the SMS 160 char limit
                if (msg.length() > 111) {
                    int length = msg.length() - 111;
                    String error = "Message is too long. Please short it by " + length + " symbols.";
                    messageBox.setError(error);
                    focusView = messageBox;
                    cancel = true;
                }

                // Number is empty; show Error
                if (number.isEmpty()) {
                    mEtNumber.setError(getString(R.string.err_empty_number));
                    focusView = mEtNumber;
                    cancel = true;
                }


                if (!PhoneNumberUtils.isWellFormedSmsAddress(number)) {
                    mEtNumber.setError(getString(R.string.err_illegal_number));
                    focusView = mEtNumber;
                    cancel = true;
                }

                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {

                    number = PhoneNumberUtils.stripSeparators(number);

                    Log.d("Number", "number: " + number);

                    askForEncryption(number, msg);


                }

            }
        });


    }

    private void askForEncryption(final String number, final String msg) {

        // check if key is already known
        final String savedKey = CryptoUtils.getStoredKey(this, number);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Encrypt");
        builder.setMessage("Do you want to send your message encrypted?");

        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
        // possible Buttons are uneffected.
        LayoutInflater inflater = getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.encrypt_dialog, null);

        final EditText keyEditText = (EditText) view.findViewById(R.id.key);
        ImageView infoView = (ImageView) view.findViewById(R.id.info);

        if (savedKey != null) {
            keyEditText.setHint("Stored key will be used");
            keyEditText.setEnabled(false);
            infoView.setVisibility(View.GONE);

            Log.d(TAG, "HashPwd found (" + savedKey.length() + "): " + savedKey);
        }


        builder.setView(view);
        builder.setPositiveButton("Send secure", null); //Set to null. We override the onclick

        builder.setNegativeButton("Send unsecure", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendSms(number, msg, false, null);

            }
        });

        infoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(WriteMessageActivity.this);
                builder.setTitle("Password restriction");

                // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
                // possible Buttons are unaffected.
                LayoutInflater inflater = getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View dialogView = inflater.inflate(R.layout.password_restriction_dialog, null);


                builder.setView(dialogView);

                builder.setNeutralButton(android.R.string.ok, null);

                builder.show();


            }
        });


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // Override onClick to prevent dialog from dismissing after clicking on BUTTON_POSITIVE (want to check keyEditText)
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String keyPlain;
                byte[] hashedKey;

                // no saved key found => ask for one
                if (savedKey == null) {
                    keyPlain = keyEditText.getText().toString();

                    // allow no empty password
                    if (keyPlain.isEmpty()) {
                        keyEditText.setError("Empty Password");
                        keyEditText.requestFocus();
                        return;
                    }

                    // check for password restrictions
                    if (!keyPlain.matches(CryptoUtils.PW_REGEX)) {
                        keyEditText.setError("Invalid password");
                        keyEditText.requestFocus();
                        return;
                    }

                    try {
                        hashedKey = CryptoUtils.hashKey(keyPlain);
                    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return;
                    }

                    CryptoUtils.storeSecretKey(WriteMessageActivity.this, number, hashedKey);

                } else {

                    // decode base64-encoded hash key
                    hashedKey = Base64.decode(savedKey, Base64.NO_WRAP);
                }

                Log.d(TAG, "Hashed key (" + hashedKey.length + "):\t" + Arrays.toString(hashedKey));

                SecretKeySpec secKey = new SecretKeySpec(hashedKey, CryptoUtils.ALGORITHM);

                // use InitialVector with first 16 bit of the hashed key so that the receiver can use the same IV
                IvParameterSpec iv = new IvParameterSpec(Arrays.copyOf(hashedKey, 16));

                byte[] encryptedBytes;
                try {
                    encryptedBytes = CryptoUtils.encrypt(msg, secKey, iv);
                } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | UnsupportedEncodingException | BadPaddingException | IllegalBlockSizeException e) {
                    String errMsg = e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
                    String toastMsg = "Error encrypting message: New Error '" + e.getLocalizedMessage() + "'";
                    Log.d(TAG, "Exception thrown while decrypting message: " + errMsg);
                    Log.d(TAG, "Showing toast: " + toastMsg);
                    Toast.makeText(WriteMessageActivity.this, toastMsg, Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                    return;

                }

                byte[] baseBytes = Base64.encode(encryptedBytes, Base64.NO_WRAP);
                String encryptedMessage = new String(baseBytes);

                Log.d(TAG, "CipherBytes:\t(" + encryptedBytes.length + ")\t" + Arrays.toString(encryptedBytes));
                Log.d(TAG, "BaseBytes:\t(" + baseBytes.length + ")\t" + Arrays.toString(baseBytes));
                Log.d(TAG, "CipherText:\t(" + encryptedMessage.length() + ")\t" + encryptedMessage);
                Log.d(TAG, "Length of encr. Message (bytes):" + encryptedMessage.getBytes().length);


                sendSms(number, msg, true, encryptedMessage);

                alertDialog.dismiss();


            }
        });


    }


    /**
     * Dummy sendSms method, would need the "to" number to actually send a message :)
     */
    private void sendSms(String number, String message, boolean encrypted, String cipherText) {
        if (!TextUtils.isEmpty(message)) {
            if (Utils.isDefaultSmsApp(this)) {

                Intent sentIntent = new Intent(MessagingService.ACTION_MESSAGE_SENT);
                sentIntent.putExtra(SmsQuery.Sent.ADDRESS_NAME, number);
                sentIntent.putExtra(SmsQuery.Sent.BODY_NAME, message);
                sentIntent.putExtra(SmsQuery.Sent.DATE_NAME, System.currentTimeMillis());
                sentIntent.putExtra(SmsQuery.Sent.ENCRYPTED, encrypted);

                PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                SmsManager smsManager = SmsManager.getDefault();

                // send cipherText if msg should be encrypted
                smsManager.sendTextMessage(number, null, encrypted ? cipherText : message, sentPI, null);

                // ContentProvider
                Toast.makeText(this, "Sending " + (encrypted ? "encrypted" : "unencrypted") + "text message: " + message, Toast.LENGTH_SHORT).show();


                // close Activity a little bit after sending
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 300);


            } else {
                // TODO: Notify the user the app is not default and provide a way to trigger
                // Utils.setDefaultSmsApp() so they can set it.
                Toast.makeText(this, "Not default", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "Loader created");

        Uri contentUri;

        // Since there's a search string, use the special content Uri that searches the
        // Contacts table. The URI consists of a base Uri and the search string.
        contentUri =
                Uri.withAppendedPath(ContactsContract.CommonDataKinds.Contactables.CONTENT_FILTER_URI, mSearchTerm);

        String selection =
                ContactsContract.CommonDataKinds.Contactables.HAS_PHONE_NUMBER + " = " + 1
                 + " AND " + ContactsContract.CommonDataKinds.Contactables.MIMETYPE + "= '" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE +  "'";


        // Sort results such that rows for the same contact stay together.
        String sortBy = ContactsContract.CommonDataKinds.Contactables.DISPLAY_NAME;


        // Returns a new CursorLoader for querying the Contacts table. No arguments are used
        // for the selection clause. The search string is either encoded onto the content URI,
        // or no contacts search string is used. The other search criteria are constants. See
        // the ContactsQuery interface.
        return new CursorLoader(this,
                contentUri,
                null,
                selection,
                null,
                sortBy);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "Loader finished");

        mAdapter.swapCursor(data);

        // only show suggestions if the search was successful
        if(data.getCount() != 0){
            mContactList.setVisibility(View.VISIBLE);
        } else {
            mContactList.setVisibility(View.GONE);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "Loader restarted");

        // When the loader is being reset, clear the cursor from the adapter. This allows the
        // cursor resources to be freed.
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mSelectContact = true;

        // Gets the Cursor object currently bound to the ListView
        final Cursor cursor = mAdapter.getCursor();

        // Moves to the Cursor row corresponding to the ListView item that was clicked
        cursor.moveToPosition(position);

        int phoneColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);


        String number = cursor.getString(phoneColumnIndex);
        mEtNumber.setText(number);

        mContactList.setVisibility(View.GONE);

        mSelectContact = false;


    }

    private class AddressWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String newText = s.toString();

            // Called when the action bar search text has changed.  Updates
            // the search filter, and restarts the loader to do a new query
            // using the new search string.
            String newFilter = !TextUtils.isEmpty(newText) ? newText : null;

            // Don't do anything if the filter is empty
            if (newFilter == null) {
                mContactList.setVisibility(View.GONE);
                return;
            }

            // Don't do anything if the new filter is the same as the current filter
            if ((mSearchTerm != null && mSearchTerm.equals(newFilter)) || mSelectContact) {
                return;
            }

            // Updates current filter to new filter
            mSearchTerm = newFilter;

            // Restarts the loader. This triggers onCreateLoader(), which builds the
            // necessary content Uri from mSearchTerm.
            getLoaderManager().restartLoader(
                    1, null, WriteMessageActivity.this);
        }

    }


}
