package at.ac.uibk.schmidstricker.securesms.sms;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Telephony;

/**
 * Created by Benedikt on 28.10.2014.
 */
public class Message implements Parcelable {

    private int mId;
    private String mAddress; //Number or Name
    private String mBody;
    private long mDate;
    private boolean mEncrypted;

    public Message(int id, String address, String body, long date, int encrypted) {
        this.mId = id;
        this.mAddress = address;
        this.mBody = body;
        this.mDate = date;
        this.mEncrypted = encrypted == 1;
    }

    public int getId() {
        return mId;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getBody() {
        return mBody;
    }

    public long getDate() {
        return mDate;
    }

    public boolean isEncrypted() {
        return mEncrypted;
    }

    public void setBody(String newBody){
        mBody = newBody;
    }

    public void setEncrypted(boolean encrypted) {
        mEncrypted = encrypted;
    }

    public ContentValues extractContentValues(){
        // Store the message in the content provider.
        ContentValues values = new ContentValues();
        values.put(SmsQuery.Inbox.ID_NAME, getId());
        values.put(SmsQuery.Inbox.ADDRESS_NAME, getAddress());
        values.put(SmsQuery.Inbox.BODY_NAME, getBody());
        values.put(SmsQuery.Inbox.DATE_NAME, getDate());
        values.put(SmsQuery.Inbox.ENCRYPTED, isEncrypted());


        return values;

    }

    public int toContentResolver(Context context){

        ContentValues values = extractContentValues();

        ContentResolver resolver = context.getContentResolver();

        return resolver.update(SmsQuery.Inbox.URI_INBOX, values, SmsQuery.Inbox.ID_NAME + " = ?", new String[]{String.valueOf(getId())});


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
