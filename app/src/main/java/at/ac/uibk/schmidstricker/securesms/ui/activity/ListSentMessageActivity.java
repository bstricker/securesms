package at.ac.uibk.schmidstricker.securesms.ui.activity;

import android.app.ListActivity;
import android.os.Bundle;

import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.adapter.MessageAdapter;
import at.ac.uibk.schmidstricker.securesms.sms.MessageLoaderCallback;
import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;


public class ListSentMessageActivity extends ListActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_sent_message);

        getListView().setEmptyView(findViewById(android.R.id.empty));


        MessageAdapter messageAdapter = new MessageAdapter(this);
        setListAdapter(messageAdapter);

        // Simple query to show the most recent SMS messages in the inbox
        getLoaderManager().initLoader(SmsQuery.Sent.TOKEN_SENT, null, new MessageLoaderCallback(this, messageAdapter));

    }





}
