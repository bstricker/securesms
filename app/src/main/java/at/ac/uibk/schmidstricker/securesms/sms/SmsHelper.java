package at.ac.uibk.schmidstricker.securesms.sms;

import android.content.ContentValues;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Benedikt on 28-Dec-14.
 */
public class SmsHelper {
    /**
     * Extract all the content values except the body from an SMS
     * message.
     */
    public static ContentValues extractContentValues(SmsMessage sms) {
        // Store the message in the content provider.
        ContentValues values = new ContentValues();
        values.put(Telephony.Sms.Inbox.ADDRESS, sms.getDisplayOriginatingAddress());
        // Use now for the timestamp to avoid confusion with clock
        // drift between the handset and the SMSC.
        // Check to make sure the system is giving us a non-bogus time.
        Calendar buildDate = new GregorianCalendar(2011, 8, 18); // 18 Sep 2011
        Calendar nowDate = new GregorianCalendar();
        long now = System.currentTimeMillis();
        nowDate.setTimeInMillis(now);
        if (nowDate.before(buildDate)) {
            // It looks like our system clock isn't set yet because the current time right now
            // is before an arbitrary time we made this build. Instead of inserting a bogus
            // receive time in this case, use the timestamp of when the message was sent.
            now = sms.getTimestampMillis();
        }
        values.put(Telephony.Sms.Inbox.DATE, new Long(now));
        values.put(Telephony.Sms.Inbox.DATE_SENT, Long.valueOf(sms.getTimestampMillis()));
        values.put(Telephony.Sms.Inbox.PROTOCOL, sms.getProtocolIdentifier());
        values.put(Telephony.Sms.Inbox.READ, 0);
        values.put(Telephony.Sms.Inbox.SEEN, 0);
        if (sms.getPseudoSubject().length() > 0) {
            values.put(Telephony.Sms.Inbox.SUBJECT, sms.getPseudoSubject());
        }
        values.put(Telephony.Sms.Inbox.REPLY_PATH_PRESENT, sms.isReplyPathPresent() ? 1 : 0);
        values.put(Telephony.Sms.Inbox.SERVICE_CENTER, sms.getServiceCenterAddress());
        return values;
    }
}
