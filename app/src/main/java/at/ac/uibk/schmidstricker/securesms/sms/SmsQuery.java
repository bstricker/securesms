package at.ac.uibk.schmidstricker.securesms.sms;

import android.net.Uri;
import android.provider.Telephony;

/**
 * Created by Benedikt on 29.10.2014.
 */
public interface SmsQuery {

    Uri CONTENT_URI = Telephony.Sms.CONTENT_URI;

    int ID = 0;
    int ADDRESS = 1;
    int BODY = 2;
    int DATE = 3;
    int ENCRYPTED = 4;

    interface Inbox {


        Uri URI_INBOX = Telephony.Sms.Inbox.CONTENT_URI;
        int TOKEN_INBOX = 1;

        String ID_NAME = Telephony.Sms.Inbox._ID;
        String ADDRESS_NAME = Telephony.Sms.Inbox.ADDRESS;
        String BODY_NAME = Telephony.Sms.Inbox.BODY;
        String DATE_NAME = Telephony.Sms.Inbox.DATE;

        String READ_NAME = Telephony.Sms.Inbox.READ;

        String SORT_ORDER = Telephony.Sms.Inbox.DEFAULT_SORT_ORDER;

        String ENCRYPTED = Telephony.Sms.LOCKED;

        String[] PROJECTION = {
                ID_NAME,
                ADDRESS_NAME,
                BODY_NAME,
                DATE_NAME,
                ENCRYPTED
        };
    }

    interface Sent {

        int TOKEN_SENT = 2;
        Uri URI_SENT = Telephony.Sms.Sent.CONTENT_URI;

        String ID_NAME = Telephony.Sms.Sent._ID;
        String ADDRESS_NAME = Telephony.Sms.Sent.ADDRESS;
        String BODY_NAME = Telephony.Sms.Sent.BODY;
        String DATE_NAME = Telephony.Sms.Sent.DATE;

        String READ_NAME = Telephony.Sms.Sent.READ;

        String SORT_ORDER = Telephony.Sms.Sent.DEFAULT_SORT_ORDER;

        String ENCRYPTED = Telephony.Sms.LOCKED;

        String[] PROJECTION = {
                ID_NAME,
                ADDRESS_NAME,
                BODY_NAME,
                DATE_NAME,
                ENCRYPTED
        };
    }
}
