package at.ac.uibk.schmidstricker.securesms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.Utils;
import at.ac.uibk.schmidstricker.securesms.sms.Message;

/**
 * Created by Benedikt on 28.10.2014.
 */
public class MessageAdapter extends ArrayAdapter<Message> {

    private final Context mContext;


    public MessageAdapter(Context context) {
        super(context, R.layout.message_list_item);
        mContext = context;
    }


    public void setData(List<Message> data) {
        clear();
        if (data != null) {
            addAll(data);
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View messageView = convertView;

        Message message = getItem(position);

        // getName returns the address to the number, or the number if no address exists
        String name = Utils.getName(message.getAddress(), mContext);

        if (messageView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            messageView = inflater.inflate(R.layout.message_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.address = (TextView) messageView.findViewById(R.id.edit_number);
            viewHolder.body = (TextView) messageView.findViewById(R.id.body);
            viewHolder.date = (TextView) messageView.findViewById(R.id.date);
            viewHolder.encrypted = (ImageView) messageView.findViewById(R.id.encrypted_flag);
            messageView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) messageView.getTag();

        Date date = new Date(message.getDate());
        holder.address.setText(name);
        holder.body.setText(message.getBody());
        holder.date.setText(Utils.DATE_FORMAT.format(date));

        if(message.isEncrypted()){
            holder.encrypted.setVisibility(View.VISIBLE);
        }

        // Returns the item layout view
        return messageView;


    }

    static class ViewHolder {
        TextView address;
        TextView body;
        TextView date;
        ImageView encrypted;
    }


}
