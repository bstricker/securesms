/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.ac.uibk.schmidstricker.securesms.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import at.ac.uibk.schmidstricker.securesms.CryptoUtils;
import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.Utils;
import at.ac.uibk.schmidstricker.securesms.sms.Message;
import at.ac.uibk.schmidstricker.securesms.adapter.MessageAdapter;
import at.ac.uibk.schmidstricker.securesms.sms.MessageLoaderCallback;
import at.ac.uibk.schmidstricker.securesms.ui.MessageNotification;
import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;

/**
 * The main Activity that provides a sample of a few things:
 * -detecting if this app is the default SMS app and then showing/hiding UI and enabling/disabling
 * functionality. the UI that is shown has a button to prompt the user to set this app as the
 * default.
 * -a simple query to the SMS content provider to show a list of SMS messages in the inbox. even
 * though the query uses KitKat APIs this query should still work on earlier versions of Android
 * as the contract class and ContentProvider were still around (with essentially the same
 * structure) but were private.
 * -being triggered from another application when creating a new SMS. a good example is creating
 * a new SMS from the system People application. although nothing is done with the incoming
 * Intent in this case (just a Toast is displayed)
 * <p/>
 * Obviously this is far from a full implementation and should just be used as a sample of how
 * an app could be set up to correctly integrate with the new Android 4.4 KitKat APIs while
 * running normally on earlier Android versions.
 */
public class MainActivity extends Activity implements AdapterView.OnItemClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private RelativeLayout mSetDefaultSmsLayout;
    private MessageAdapter mAdapter;
    private ImageButton mNewSmsButton;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find some views
        mSetDefaultSmsLayout = (RelativeLayout) findViewById(R.id.set_default_sms_layout);
        mNewSmsButton = (ImageButton) findViewById(R.id.new_sms_button);
        mNewSmsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isDefaultSmsApp(MainActivity.this)) {
                    Toast.makeText(MainActivity.this, R.string.set_default_sms_text, Toast.LENGTH_LONG).show();
                } else {

                    Intent intent = new Intent(MainActivity.this, WriteMessageActivity.class);
                    startActivity(intent);

                }


            }
        });
        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setEmptyView(findViewById(android.R.id.empty));

        mListView.setOnItemClickListener(this);

        mAdapter = new MessageAdapter(this);
        mListView.setAdapter(mAdapter);

        // Placeholder to process incoming SEND/SENDTO intents
        String intentAction = getIntent() == null ? null : getIntent().getAction();
        if (!TextUtils.isEmpty(intentAction) && (Intent.ACTION_SENDTO.equals(intentAction)
                || Intent.ACTION_SEND.equals(intentAction))) {
            // TODO: Handle incoming SEND and SENDTO intents by pre-populating UI components
            Toast.makeText(this, "Handle SEND and SENDTO intents: " + getIntent().getDataString(),
                    Toast.LENGTH_SHORT).show();
        }

        // Simple query to show the most recent SMS messages in the inbox
        getLoaderManager().initLoader(SmsQuery.Inbox.TOKEN_INBOX, null, new MessageLoaderCallback(this, mAdapter));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MessageNotification.NEW_MESSAGE_NOTIFICATION) {
            Log.d(TAG, "Got New SMS => Update list");
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Only do these checks/changes on KitKat+, the "mSetDefaultSmsLayout" has its visibility
        // set to "gone" in the xml layout so it won't show at all on earlier Android versions.
        if (Utils.hasKitKat()) {
            if (Utils.isDefaultSmsApp(this)) {
                // This app is the default, remove the "make this app the default" layout and
                // enable message sending components.
                mSetDefaultSmsLayout.setVisibility(View.GONE);
                mNewSmsButton.setActivated(true);
            } else {
                // Not the default, show the "make this app the default" layout and disable
                // message sending components.
                mSetDefaultSmsLayout.setVisibility(View.VISIBLE);

                Button button = (Button) findViewById(R.id.set_default_sms_button);
                button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.setDefaultSmsApp(MainActivity.this);
                        mNewSmsButton.setActivated(true);
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_go_to_sent: {
                Intent intent = new Intent(this, ListSentMessageActivity.class);
                startActivity(intent);
                return true;
            }

            case R.id.action_manage_keys: {
                Intent intent = new Intent(this, ManageKeysActivity.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {
        Log.d(TAG, "Item: " + position + "  " + id);

        // get message from list
        final Message message = (Message) mListView.getItemAtPosition(position);
        final String number = message.getAddress();

        // if message is stored as encrypted (has been already decrypted) don't encrypt it again...
        boolean isDecrypted = message.isEncrypted();

        if (isDecrypted) {
            Toast.makeText(this, "Message is already decrypted!", Toast.LENGTH_SHORT).show();
            return;
        }

        // check if key is already known
        final String savedKey = CryptoUtils.getStoredKey(this, number);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Decrypt");
        builder.setMessage("Do you want to decrypt this message?");

        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
        // possible Buttons are unaffected.
        LayoutInflater inflater = getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View dialogView = inflater.inflate(R.layout.encrypt_dialog, null);

        final EditText keyEditText = (EditText) dialogView.findViewById(R.id.key);

        ImageView infoView = (ImageView) dialogView.findViewById(R.id.info);




        if (savedKey != null) {
            keyEditText.setHint("Stored key will be used");
            keyEditText.setEnabled(false);

            infoView.setVisibility(View.GONE);

            Log.d(TAG, "HashPwd found (" + savedKey.length() + "): " + savedKey);
        }


        builder.setView(dialogView);
        builder.setPositiveButton(android.R.string.yes, null);
        builder.setNegativeButton(android.R.string.no, null);

        infoView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Password restriction");

                // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and
                // possible Buttons are unaffected.
                LayoutInflater inflater = getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View dialogView = inflater.inflate(R.layout.password_restriction_dialog, null);


                builder.setView(dialogView);

                builder.setNeutralButton(android.R.string.ok, null);

                builder.show();


            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();


        // Override onClick to prevent dialog from dismissing after clicking on BUTTON_POSITIVE (want to check keyEditText)
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                String keyPlain;
                byte[] hashedKey;

                if (savedKey == null) {

                    keyPlain = keyEditText.getText().toString();

                    // allow no empty password
                    if (keyPlain.isEmpty()) {
                        keyEditText.setError(getString(R.string.err_empty_msg));
                        keyEditText.requestFocus();
                        return;
                    }

                    // check for password restrictions
                    if(!keyPlain.matches(CryptoUtils.PW_REGEX)){
                        keyEditText.setError("Invalid password");
                        keyEditText.requestFocus();
                        return;
                    }

                    try {
                        // hash the plain key passphrase because AES cipher needs a 128 byte long key
                        hashedKey = CryptoUtils.hashKey(keyPlain);
                    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return;
                    }

                    CryptoUtils.storeSecretKey(MainActivity.this, number, hashedKey);


                } else {
                    // decode base64-encoded hash key
                    hashedKey = Base64.decode(savedKey, Base64.NO_WRAP);

                }

                Log.d(TAG, "Hashed key (" + hashedKey.length + "):\t" + Arrays.toString(hashedKey));

                SecretKeySpec secKey = new SecretKeySpec(hashedKey, CryptoUtils.ALGORITHM);

                String encryptedMessage = message.getBody();

                String errMsg = null;
                String toastMsg = null;

                byte[] decrypted = null;
                try {

                    byte[] decodedBytes = Base64.decode(encryptedMessage, Base64.NO_WRAP);

                    Log.d(TAG, "CipherText:\t" + encryptedMessage);

                    // use InitialVector with first 16 bit of the hashed key to ensure that it's the same as sender's IV
                    IvParameterSpec iv = new IvParameterSpec(Arrays.copyOf(hashedKey, 16));

                    // decrypt message
                    decrypted = CryptoUtils.decrypt(secKey, decodedBytes, iv);

                } catch (BadPaddingException e) {
                    errMsg = e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
                    toastMsg = "Error encrypting message. Maybe wrong key?";
                } catch (IllegalArgumentException | IllegalBlockSizeException e) {
                    errMsg = e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
                    toastMsg = "This message isn't encrypted (anymore).";
                } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException e) {
                    errMsg = e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
                    toastMsg = "Error encrypting message: New Error '" + e.getLocalizedMessage() + "'";
                }

                // got an error with encrypting => show toast and close dialog
                if (toastMsg != null) {
                    Log.d(TAG, "Exception thrown while decrypting message: " + errMsg);
                    Log.d(TAG, "Showing toast: " + toastMsg);
                    Toast.makeText(MainActivity.this, toastMsg, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    return;
                }


                String decryptedString = new String(decrypted);

                Log.d(TAG, "Decrypted:\t" + Arrays.toString(decrypted));
                Log.d(TAG, "DecryptedString:\t" + decryptedString);

                message.setBody(decryptedString);
                message.setEncrypted(true);

                // update encrypted message in SMS database
                int updatedRows = message.toContentResolver(MainActivity.this);

                Log.d(TAG, updatedRows + " rows updated");

                mAdapter.notifyDataSetChanged();

                dialog.dismiss();
            }
        });

    }
}
