package at.ac.uibk.schmidstricker.securesms.ui.activity;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Pair;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import at.ac.uibk.schmidstricker.securesms.CryptoUtils;
import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.adapter.KeyAdapter;
import at.ac.uibk.schmidstricker.securesms.ui.SwipeDismissListViewTouchListener;


public class ManageKeysActivity extends ListActivity {

    private static final String TAG = ManageKeysActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // store newly defined key to use it next time with this number
        SharedPreferences prefs = this.getSharedPreferences(CryptoUtils.SECRET_KEYS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        setContentView(R.layout.activity_manage_keys);

        Map<String, ?> all = prefs.getAll();


        List<Pair<String, String>> entries = new ArrayList<>(all.size());

        for (Map.Entry<String, ?> entry : all.entrySet()) {

            entries.add(new Pair<>(entry.getKey(), entry.getValue().toString()));

        }

        final KeyAdapter adapter = new KeyAdapter(this, R.layout.key_list_item, entries);

        ListView listView = getListView();
        listView.setAdapter(adapter);


        SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(listView, new SwipeDismissListViewTouchListener.DismissCallbacks() {

            @Override
            public boolean canDismiss(int position) {
                return true;
            }

            @Override
            public void onDismiss(ListView listView, int[] reverseSortedPositions) {


                for (int position : reverseSortedPositions) {
                    Pair<String, String> numberKeyPair = adapter.getItem(position);
                    adapter.remove(numberKeyPair);
                    editor.remove(numberKeyPair.first);
                    Toast.makeText(ManageKeysActivity.this, "Key of " + numberKeyPair.first + " removed", Toast.LENGTH_SHORT).show();
                    editor.apply();
                }

                adapter.notifyDataSetChanged();

            }
        });

        listView.setOnTouchListener(touchListener);
        listView.setOnScrollListener(touchListener.makeScrollListener());


    }


}
