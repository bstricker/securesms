/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.ac.uibk.schmidstricker.securesms;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.provider.Telephony.Sms.Intents;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;

public class Utils {

    public static final  DateFormat    DATE_FORMAT    = DateFormat.getDateTimeInstance();

    /**
     * Check if the device runs Android 4.3 (JB MR2) or higher.
     */
    public static boolean hasJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR2;
    }

    /**
     * Check if the device runs Android 4.4 (KitKat) or higher.
     */
    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;
    }

    /**
     * Check if your app is the default system SMS app.
     * @param context The Context
     * @return True if it is default, False otherwise. Pre-KitKat will always return True.
     */
    @TargetApi(VERSION_CODES.KITKAT)
    public static boolean isDefaultSmsApp(Context context) {
        if (hasKitKat()) {
            return context.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(context));
        }

        return true;
    }

    /**
     * Trigger the intent to open the system dialog that asks the user to change the default
     * SMS app.
     * @param context The Context
     */
    @TargetApi(VERSION_CODES.KITKAT)
    public static void setDefaultSmsApp(Context context) {
        // This is a new intent which only exists on KitKat
        if (hasKitKat()) {
            Intent intent = new Intent(Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Intents.EXTRA_PACKAGE_NAME, context.getPackageName());
            context.startActivity(intent);
        }
    }



    /**
     * Returns the address of the specified number using the optimized lookup table PhoneLookup.
     * If no address matches to the given number, null will be returned. If the number is linked
     * with more
     * than one contact, then it's not determined which address is returned.
     *
     * @param number Telephone number to look up the address
     *
     * @return The address to the specified number, or the number if no address exists for this number.
     */
    public static String getName(String number, Context context) {


        if (context == null) {
            throw new IllegalArgumentException("ContentResolver must not be null");
        }

        if (number == null) {
            throw new IllegalArgumentException("Number must not be null");
        }

        String name = number;
        String[] projection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup
                .DISPLAY_NAME, ContactsContract.PhoneLookup.NUMBER};

        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));

        Cursor c = context.getContentResolver().query(lookupUri, projection, null, null, null);

        while (c.moveToNext()) {
            name = c.getString(c.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        c.close();

        return name;
    }

    public static String[] toHexArray(byte[] bytes){

        String[] ret = new String[bytes.length];

        for(int i = 0; i < bytes.length; i++){
            ret[i] = String.format("%02x", bytes[i]);
        }

        return ret;

    }



}
