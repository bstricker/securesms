/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.ac.uibk.schmidstricker.securesms.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import at.ac.uibk.schmidstricker.securesms.ui.MessageNotification;
import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.sms.SmsHelper;
import at.ac.uibk.schmidstricker.securesms.sms.SmsQuery;
import at.ac.uibk.schmidstricker.securesms.Utils;
import at.ac.uibk.schmidstricker.securesms.receiver.MessagingReceiver;


/**
 * This service is triggered internally only and is used to process incoming SMS and MMS messages
 * that the {@link at.ac.uibk.schmidstricker.securesms.receiver.MessagingReceiver} passes over. It's
 * preferable to handle these in a service in case there is significant work to do which may exceed
 * the time allowed in a receiver.
 */
public class MessagingService extends IntentService {
    // These actions are for this app only and are used by MessagingReceiver to start this service
    public static final String ACTION_MY_RECEIVE_SMS = "com.example.android.smssample.RECEIVE_SMS";
    public static final String ACTION_MY_RECEIVE_MMS = "com.example.android.smssample.RECEIVE_MMS";
    public static final String ACTION_MESSAGE_SENT = "com.example.android.smssample.MESSAGE_SENT";
    public static final String ACTION_MESSAGE_DELIVERED = "com.example.android.smssample.MESSAGE_DELIVERED";


    private static final String TAG = "MessagingService";

    // This must match the column IDs below.
    private final static String[] REPLACE_PROJECTION = new String[]{
            Telephony.Sms._ID,
            Telephony.Sms.ADDRESS,
            Telephony.Sms.PROTOCOL
    };
    // This must match REPLACE_PROJECTION.
    private static final int REPLACE_COLUMN_ID = 0;

    public MessagingService() {
        super(TAG);
    }

    public static String replaceFormFeeds(String s) {
// Some providers send formfeeds in their messages. Convert those formfeeds to newlines.
        return s == null ? "" : s.replace('\f', '\n');
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.d(TAG, "New Intent: " + intent.toString());
            String intentAction = intent.getAction();


            if (ACTION_MY_RECEIVE_SMS.equals(intentAction)) {
                // TODO: Handle incoming SMS

                Log.d(TAG, "New SMS!");

                int error = intent.getIntExtra("errorCode", 0);
                handleSmsReceived(intent, error);

                // Ensure wakelock is released that was created by the WakefulBroadcastReceiver
                MessagingReceiver.completeWakefulIntent(intent);
            } else if (ACTION_MY_RECEIVE_MMS.equals(intentAction)) {
                // TODO: Handle incoming MMS

                Log.d(TAG, "New MMS!");

                // Ensure wakelock is released that was created by the WakefulBroadcastReceiver
                MessagingReceiver.completeWakefulIntent(intent);
            } else if (ACTION_MESSAGE_SENT.equals(intentAction)) {

                Log.d(TAG, "SMS sent!");


                handleSmsSent(intent);

                MessagingReceiver.completeWakefulIntent(intent);

            }
        }
    }

    private void handleSmsSent(Intent intent) {

        Bundle extras = intent.getExtras();
        String address = extras.getString(SmsQuery.Sent.ADDRESS_NAME);
        String body = extras.getString(SmsQuery.Sent.BODY_NAME);
        long date = extras.getLong(SmsQuery.Sent.DATE_NAME);
        boolean encrypted = extras.getBoolean(SmsQuery.Sent.ENCRYPTED);

        ContentResolver resolver = getContentResolver();
        ContentValues values = new ContentValues();

        values.put(SmsQuery.Sent.ADDRESS_NAME, address);
        values.put(SmsQuery.Sent.BODY_NAME, body);
        values.put(SmsQuery.Sent.DATE_NAME, date);
        values.put(SmsQuery.Sent.ENCRYPTED, encrypted);

        Uri messageUri = resolver.insert(SmsQuery.Sent.URI_SENT, values);

        Log.v(TAG, "handleSmsSent" + " messageUri: " + messageUri +
                ", address: " + address +
                ", body: " + body);

    }

    private void handleSmsReceived(Intent intent, int error) {
        SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        String format = intent.getStringExtra("format");
        Uri messageUri = insertMessage(this, msgs, error, format);

        SmsMessage sms = msgs[0];
        Log.v(TAG, "handleSmsReceived" + (sms.isReplace() ? "(replace)" : "") +
                " messageUri: " + messageUri +
                ", address: " + sms.getOriginatingAddress() +
                ", body: " + sms.getMessageBody());

        if (messageUri != null) {
            Log.d(TAG, "handleSmsReceived messageUri: " + messageUri);

            MessageNotification.newMessage(this, messageUri);

        }
    }

    /**
     * If the message is a class-zero message, display it immediately
     * and return null. Otherwise, store it using the
     * <code>ContentResolver</code> and return the
     * <code>Uri</code> of the thread containing this message
     * so that we can use it for notification.
     */
    private Uri insertMessage(Context context, SmsMessage[] msgs, int error, String format) {
    // Build the helper classes to parse the messages.
        return storeMessage(context, msgs, error);

    }


    private Uri storeMessage(Context context, SmsMessage[] msgs, int error) {
        SmsMessage sms = msgs[0];
        // Store the message in the content provider.
        ContentValues values = SmsHelper.extractContentValues(sms);
        values.put(Telephony.Sms.ERROR_CODE, error);
        int pduCount = msgs.length;
        if (pduCount == 1) {
            // There is only one part, so grab the body directly.
            values.put(Telephony.Sms.Inbox.BODY, replaceFormFeeds(sms.getDisplayMessageBody()));
        } else {
            // Build up the body from the parts.
            StringBuilder body = new StringBuilder();
            for (int i = 0; i < pduCount; i++) {
                sms = msgs[i];
                body.append(sms.getDisplayMessageBody());
            }
            values.put(Telephony.Sms.Inbox.BODY, replaceFormFeeds(body.toString()));
        }
        // Make sure we've got a thread id so after the insert we'll be able to delete
        // excess messages.
        Long threadId = values.getAsLong(Telephony.Sms.THREAD_ID);
        String address = values.getAsString(Telephony.Sms.ADDRESS);
        // Code for debugging and easy injection of short codes, non email addresses, etc.
        // See Contact.isAlphaNumber() for further comments and results.
        // switch (count++ % 8) {
        // case 0: address = "AB12"; break;
        // case 1: address = "12"; break;
        // case 2: address = "Jello123"; break;
        // case 3: address = "T-Mobile"; break;
        // case 4: address = "Mobile1"; break;
        // case 5: address = "Dogs77"; break;
        // case 6: address = "****1"; break;
        // case 7: address = "#4#5#6#"; break;
        // }
        if (!TextUtils.isEmpty(address)) {
            address = Utils.getName(address, this);
        } else {
            address = getString(R.string.unknown_sender);
            values.put(Telephony.Sms.ADDRESS, address);
        }
        if (((threadId == null) || (threadId == 0)) && (address != null)) {
            // threadId = Conversation.getOrCreateThreadId(context, address);
            // values.put(Telephony.Sms.THREAD_ID, threadId);
            Log.e(TAG, "threadId is null");
        }
        ContentResolver resolver = context.getContentResolver();

        return resolver.insert(SmsQuery.Inbox.URI_INBOX, values);
    }

}
