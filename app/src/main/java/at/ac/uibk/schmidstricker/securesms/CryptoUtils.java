package at.ac.uibk.schmidstricker.securesms;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Benedikt on 28-Dec-14.
 */
public class CryptoUtils {
    public static final String SECRET_KEYS = "at.ac.uibk.schmidstricker.securesms.SECRET_KEYS";
    public static final String ALGORITHM = "AES";
    public static final String MODE = "CBC";
    public static final String PADDING = "PKCS5Padding";
    public static final String CIPHER = ALGORITHM + "/" + MODE + "/" + PADDING;

    // modified version of http://stackoverflow.com/a/3802238
    public static final String PW_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,24}$";

    public static byte[] hashKey(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes("UTF-8"));
        return md.digest();
    }

    public static byte[] encrypt(String plainText, SecretKeySpec sks, IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        // Encode the original data with AES
        Cipher c = Cipher.getInstance(CIPHER);
        c.init(Cipher.ENCRYPT_MODE, sks, iv);
        return c.doFinal(plainText.getBytes("UTF-8"));

    }

    public static byte[] decrypt(SecretKeySpec sks, byte[] encodedBytes, IvParameterSpec iv) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        // Decode the encoded data with AES
        Cipher c = Cipher.getInstance(CIPHER);
        c.init(Cipher.DECRYPT_MODE, sks, iv);
        return c.doFinal(encodedBytes);
    }

    /**
     * Return the stored key for the given number.
     * <p/>
     * Checks if a hashed secret key has been stored for this particular number. If no key is stored, {@code null} will be returned.
     * <p/>
     * SecureSMS uses SharedPreferences to store the key for each number.
     *
     * @param context Context to get the SharedPreferences of the app
     * @param number  Number for which the key should be returned
     * @return stored hashed secret key of the given number, or {@code null} if no key has been found
     */
    public static String getStoredKey(Context context, String number) {
        // check if a key for this number has been stored
        SharedPreferences prefs = context.getSharedPreferences(CryptoUtils.SECRET_KEYS, Context.MODE_PRIVATE);
        return prefs.getString(number, null);
    }


    public static void storeSecretKey(Context context, String number, byte[] hashedKey) {
        // store newly defined key to use it next time with this number
        SharedPreferences prefs = context.getSharedPreferences(CryptoUtils.SECRET_KEYS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        String hashKey = Base64.encodeToString(hashedKey, Base64.NO_WRAP);

        editor.putString(number, hashKey);
        editor.apply();

    }
}
