package at.ac.uibk.schmidstricker.securesms.adapter;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.ac.uibk.schmidstricker.securesms.R;
import at.ac.uibk.schmidstricker.securesms.Utils;

/**
 * Created by Benedikt on 23.01.2015.
 */
public class KeyAdapter extends ArrayAdapter<Pair<String, String>> {

    private final Context mContext;
    private final int mResourceId;

    public KeyAdapter(Context context, int resource, List<Pair<String, String>> objects) {
        super(context, resource, objects);
        mContext = context;
        mResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View keyView = convertView;

        Pair<String, String> numberKeyPair = getItem(position);

        // getName returns the address to the number, or the number if no address exists
        String name = Utils.getName(numberKeyPair.first, mContext);

        if (keyView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            keyView = inflater.inflate(mResourceId, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.address = (TextView) keyView.findViewById(android.R.id.text1);
            viewHolder.hashedKey = (TextView) keyView.findViewById(android.R.id.text2);
            keyView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) keyView.getTag();
        holder.address.setText(name);
        holder.hashedKey.setText(numberKeyPair.second);

        // Returns the item layout view
        return keyView;


    }

    static class ViewHolder {
        TextView address;
        TextView hashedKey;
    }
}
